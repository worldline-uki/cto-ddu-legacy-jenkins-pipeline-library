#!/usr/bin/env groovy

/**
 * Deploy versioned services to docker swarm for specified environment
 */
def call (String dockerStackId="coeus", String environment = "") {

    // def gitRepo = sh returnStdout: true, script: "echo `git config remote.origin.url`  | sed 's~http[s]*://~~g'  | tr -d '\n'"
    // sh "git merge origin/master"
    // sh "git commit --no-edit || echo 'Nothing to commit'"
    // sh "git push https://\${GIT_USERNAME}:\${GIT_PASSWORD}@${gitRepo} || echo 'Push not required'"

    def targetEnv
    if (environment == "") {
        println("Branch is ${env.BRANCH_NAME}")
        def environmentString = "${env.BRANCH_NAME}" as String
        // Get environment from branch name
        targetEnv = (environmentString =~ /(.*)-environment/)[0][1]
    } else {
        targetEnv = environment as String
    }
    println("Target environment is ${targetEnv}")

    // Get server address from environment.json
    def environmentConfig = readJSON file: 'environments.json'
    def deploymentServer = environmentConfig.get(targetEnv).server
    println("Deployment server is ${deploymentServer}")

    sh """
        export \$(grep -v '^#' ./version/service-versions)
        docker-compose -f ./docker/docker-compose.base.yml -f ./docker/docker-compose.${targetEnv}.yml config > docker-stack.yml
        scp -i /var/lib/jenkins/.ssh/id_rsa docker-stack.yml jenkins@${deploymentServer}:./
        ssh jenkins@${deploymentServer} -i /var/lib/jenkins/.ssh/id_rsa -o StrictHostKeyChecking=no \
          'docker stack deploy --compose-file docker-stack.yml --with-registry-auth ${dockerStackId}'
    """
}
