#!/usr/bin/env groovy

/**
 * Bump version number in build file
 */
def call (String buildFilePath, String bump, String service = "") {

    // Identify the type of version file being processed
    def versionIdentifierRegex
    switch (buildFilePath.substring(buildFilePath.lastIndexOf('.')+1)) {
        case "sbt":
            versionIdentifierRegex = "(\\s*version\\s*:=\\s*\")([\\d.]+)(\"\\s*)"
            break
        case "json":
            versionIdentifierRegex = "(\\s*\"version\"\\s*:\\s*\")([\\d.]+)(\"\\s*,?\\s*)"
            break
    }

    def buildFile = readFile(buildFilePath)

    // Find the current version number in the build file
    def currentVersion = (buildFile =~ /${versionIdentifierRegex}/)[0][2]
    println "Current version is " + currentVersion
    
    // Determine the content for the tag 
    def tagContent
    if (service == "") {
        tagContent = currentVersion
    } else {
        tagContent = "${service}-${currentVersion}"        
    }

    // Tag the current version in GitHub
    def gitRepo = sh returnStdout: true, script: "echo `git config remote.origin.url`  | sed 's~http[s]*://~~g'  | tr -d '\n'"
    sh "git tag -a ${tagContent} -m \"Jenkins - Tagged version ${tagContent}\"" 
    sh "git push https://${gitRepo} --tag '${tagContent}'"

    // Extract the major/minor/patch number and bump
    def major=currentVersion.substring(0, currentVersion.indexOf('.')).toInteger()
    def minor=currentVersion.substring(currentVersion.indexOf('.')+1, currentVersion.lastIndexOf('.')).toInteger()
    def patch=currentVersion.substring(currentVersion.lastIndexOf('.')+1).toInteger()

    switch (bump) {
        case "PATCH":
            patch = patch + 1
            break
        case "MINOR":
            minor = minor + 1
            patch = 0
            break
        case "MAJOR":
            major = major + 1
            minor = 0
            patch = 0
            break
    }

    def newVersion = major + "." + minor + "." + patch
    println "New version (${bump}) is " + newVersion

    // Replace the version number
    def newbuildFile = buildFile.replaceFirst(~/${versionIdentifierRegex}/, '$1' + newVersion + '$3')

    // Write the new version back to the version file
    writeFile([file: buildFilePath, text: newbuildFile])

    // Determine the content for the tag 
    def commitMessage
    if (service == "") {
        commitMessage = "Updated version from ${currentVersion} to new ${bump} version ${newVersion}"
    } else {
        commitMessage = "Updated ${service} version from ${currentVersion} to new ${bump} version ${newVersion}"
    }

    // Commit the updated version file to Git
    sh "git add ${buildFilePath}"
    sh "git commit -m \"${commitMessage}\""
    sh "git push https://${gitRepo} HEAD:master"

}
