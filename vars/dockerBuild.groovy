#!/usr/bin/env groovy

/**
 * Build and deploy docker container
 */
def call (String serviceName, String buildFile, String path = "") {

    def buildFilePath
    def buildPath
    if (path == "") {
        buildFilePath = buildFile
        buildPath = "."
    } else {
        if (buildFile.indexOf("/") == -1) {
            // Build file does not contain path so prepend provided path
            buildFilePath = "./" + path + "/" + buildFile        
            buildPath = "./" + path            
        } else {
            // Build file already contains path so just use provided value
            buildFilePath = buildFile        
            buildPath = "./" + path
        }        
    }

    // Identify the type of version file being processed
    def versionIdentifierRegex
    switch (buildFile.substring(buildFile.lastIndexOf('.')+1)) {
        case "sbt":
            versionIdentifierRegex = "(\\s*version\\s*:=\\s*\")([\\d.]+)(\"\\s*)"
            break
        case "json":
            versionIdentifierRegex = "(\\s*\"version\"\\s*:\\s*\")([\\d.]+)(\"\\s*,?\\s*)"
            break
    }

    def buildFileContent = readFile(buildFilePath)

    // Find the current version number in the build file
    def currentVersion = (buildFileContent =~ /${versionIdentifierRegex}/)[0][2]
    println "Current version is " + currentVersion

    // Write the version to a file for future jenkins build steps
    writeFile([file: "docker.buildversion", text: currentVersion])

    sh "docker build --no-cache --force-rm --build-arg DOCKERREPO=localhost:8082 --build-arg USENEXUS=true -t ${serviceName} ${buildPath}"
    sh "docker tag ${serviceName} localhost:8083/${serviceName}:${currentVersion}"
    sh "docker push localhost:8083/${serviceName}:${currentVersion}"
    sh "docker rmi ${serviceName}"
    sh "docker rmi \$(docker images | grep \"${serviceName}\" | awk '{print \$3}')"

}
